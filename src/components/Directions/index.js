import React from "react";
import MapViewDirections from "react-native-maps-directions";

const Directions = ({ destination, origin, onReady }) => (
  <MapViewDirections
    destination={destination}
    origin={origin}
    onReady={onReady}
    apikey="AIzaSyBQ0KkBqMi1YCgYiaXrJkDAtR5TfafKN6k"
    strokeWidth={3}
    strokeColor="#222"
  />
);

export default Directions;